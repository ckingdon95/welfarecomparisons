    
numeraire='Rice'

# -*- coding: utf-8 -*-
import tempfile
import numpy as np
import pandas as pd
from numpy.linalg import norm
import sys
#sys.path.append('../Empirics')
import cfe as nd
from ghana_data_flexible import y,z
from cfe.df_utils import df_to_orgtbl

z['Children'] = z[['Boys','Girls']].sum(axis=1)
z['Adults'] = z[['Men','Women']].sum(axis=1)
z = z[['Adults','Children','log HSize']]

#Drop goods with too few observations
use=y.columns[y.count()>50]
y=y[use]

#Estimate the reduced form
a,ce,d,se=nd.estimation.estimate_reduced_form(y,z,return_se=True)

#Estimate the elasticities and normalize by numeraire
bphi,logL = nd.estimation.get_loglambdas(ce)
bphi_normalized = pd.DataFrame(bphi/bphi[numeraire])
goodsdf = pd.concat([bphi_normalized,d],axis=1)

#Compute standard errors
bphi_se=pd.DataFrame(nd.estimation.bootstrap_elasticity_stderrs(ce))
stderr = pd.concat([bphi_se,se],axis=1)
stderr.rename(index=str, columns = {0:goodsdf.columns[0]},inplace=True)

print df_to_orgtbl(goodsdf,sedf=stderr)


#Now do it for each round separately  
#(Note: this code was to figure out what was making cassava and chicken ill behaved, 
#not replicating a result that can be found in the paper!)
rounds = ['baseline','midline1','midline2','midline3','endline','followup']
     
for i in range(0,6):
    y_sub = y.loc[(slice(None), [rounds[i]], slice(None)), :]
    z_sub = z.loc[(slice(None), [rounds[i]], slice(None)), :]
    use_sub = y_sub.columns[y_sub.count()>15]
    y_sub = y_sub[use_sub]
    a_sub,ce_sub,d_sub,se_sub=nd.estimation.estimate_reduced_form(y_sub,z_sub,return_se=True)
    bphi_sub,logL_sub = nd.estimation.get_loglambdas(ce_sub)
    print bphi_sub
    
#What's going on with midline2?
y_sub = y.loc[(slice(None), [rounds[2]], slice(None)), :]
z_sub = z.loc[(slice(None), [rounds[2]], slice(None)), :]
use_sub = y_sub.columns[y_sub.count()>15]
y_sub = y_sub[use_sub]
a_sub,ce_sub,d_sub,se_sub=nd.estimation.estimate_reduced_form(y_sub,z_sub,return_se=True)
bphi_sub,logL_sub = nd.estimation.get_loglambdas(ce_sub)

