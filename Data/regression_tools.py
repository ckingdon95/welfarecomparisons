
import numpy as np
import pandas as pd
import statsmodels.api as sm
import sys
#import neediness as nd
import cfe.estimation as nd
from oct2py import octave
octave.addpath('../utils/IncPACK')
from matplotlib import pyplot as plt

def topcode(Series, **kwargs):
    """
    Need to implement two-sided censoring as well.
    WARNING: if Top<0, all zeros will be changed to Top
    """

    percent    = kwargs.setdefault("percent",99)
    stdev      = kwargs.setdefault("stdev",False)
    drop       = kwargs.setdefault("drop",False)
    drop_zeros = kwargs.setdefault("drop_zeros",True)
    twoway     = kwargs.setdefault("twoway",False)

    if drop_zeros: S = Series.replace(0,np.nan).dropna()
    else: S = Series.dropna()
    N_OBS = S.notnull().sum()
    if N_OBS<10: return S

    if percent: Top = np.percentile(S, percent)
    if stdev:   
        Top =  S.dropna().mean()
        Top += stdev*S.dropna().std()
    try: assert((not drop_zeros) or Top>0)
    except AssertionError: raise ValueError("Top < 0 but zeros excluded")
    if drop: replace_with = np.nan
    else:    replace_with = Top
    Series[Series>Top]=replace_with

    if not twoway: return Series
    else:
        kwargs['twoway'] = False
        return -1*topcode(-1*Series, **kwargs)
def sqr(List, width=4):
    """
    Returns a rectangular string representation of List
    For easy reading in interactive mode
    if List is a pd.DataFrame, returns sqr of columns
    """
    try: List=List.columns.tolist()
    except AttributeError: pass
    lines = []
    for i in range(int(len(List)/width)+1):
        sublist= List[width*i:width*(i+1)]
        lines.append(", ".join(map(str,sublist)))
    square = "\n".join(lines)
    print(square)
def small(df,n=4,m=None):
    """
    Returns top n rows and m columns of a df or Series for viewing
    like pd.head(), but gives a subset of columns
    if n or m are negative, gives bottom rows and last columns
    """
    print("Shape: {}".format(repr(df.shape)))
    if m is None: m=n
    if (len(df.shape)==1):
        if n>0: return df.iloc[:n]
        else: return df.iloc[n:]
    if (n>0) and (m>0): return df.iloc[:n,:m]
    else: return df.iloc[n:,m:]
def winsorize(Series, **kwargs):
    """
    Need to implement two-sided censoring as well.
    WARNING: if Top<0, all zeros will be changed to Top
    """

    percent    = kwargs.setdefault("percent",99)
    stdev      = kwargs.setdefault("stdev",False)
    drop       = kwargs.setdefault("drop",False)
    drop_zeros = kwargs.setdefault("drop_zeros",True)
    twoway     = kwargs.setdefault("twoway",False)

    if drop_zeros: S = Series.replace(0,np.nan).dropna()
    else: S = Series.dropna()
    N_OBS = S.notnull().sum()
    if N_OBS<10: return S

    if percent: Top = np.percentile(S, percent)
    if stdev:   
        Top =  S.dropna().mean()
        Top += stdev*S.dropna().std()
    try: assert((not drop_zeros) or Top>0)
    except AssertionError: raise ValueError("Top < 0 but zeros excluded")
    if drop: replace_with = np.nan
    else:    replace_with = Top
    Series[Series>Top]=replace_with

    if not twoway: return Series
    else:
        kwargs['twoway'] = False
        return -1*topcode(-1*Series, **kwargs)
def group_expenditures(df,groups):
    X=pd.DataFrame(index=df.index)
    for k,v in groups.iteritems():
        X[k]=df[v].sum(axis=1)
            
    return X
def df_to_orgtbl(df,tdf=None,sedf=None,float_fmt='%5.3f'):
    """
    Print pd.DataFrame in format which forms an org-table.
    Note that headers for code block should include ':results table raw'.
    """
    if len(df.shape)==1: # We have a series?
       df=pd.DataFrame(df)

    if (tdf is None) and (sedf is None):
        return '|'+df.to_csv(sep='|',float_format=float_fmt,line_terminator='|\n|')
    elif not (tdf is None) and (sedf is None):
        s = '| |'+'|  '.join(df.columns)+' |\n|-\n'
        for i in df.index:
            s+='| %s ' % i
            for j in df.columns:
                try:
                    stars=(np.abs(tdf.loc[i,j])>1.65) + 0.
                    stars+=(np.abs(tdf.loc[i,j])>1.96) + 0.
                    stars+=(np.abs(tdf.loc[i,j])>2.577) + 0.
                    if stars>0:
                        stars='^{'+'*'*stars + '}'
                    else: stars=''
                except KeyError: stars=''
                if np.isnan(df.loc[i,j]): entry='| $ $ '
                else: entry='| $'+float_fmt+stars+'$ '
                s+=entry % df.loc[i,j]
            s+='|\n'
        return s

    elif not sedf is None: # Print standard errors on alternate rows
        s = '| |'+'|  '.join(df.columns)+' |\n|-\n'
        for i in df.index:
            s+='| %s ' % i
            for j in df.columns: # Point estimates
                try:
                    stars = (np.abs(df.loc[i,j]/sedf.loc[i,j])>1.65) + 0.
                    stars+= (np.abs(df.loc[i,j]/sedf.loc[i,j])>1.96) + 0.
                    stars+= (np.abs(df.loc[i,j]/sedf.loc[i,j])>2.577) + 0.
                    if stars>0:
                        stars='^{'+'*'*stars + '}'
                    else: stars=''
                except KeyError: stars=''
                if np.isnan(df.loc[i,j]): entry='| $ $ '
                else: entry='| $'+float_fmt+stars+'$ '
                s+=entry % df.loc[i,j]
            s+='|\n|'
            for j in df.columns: # Now standard errors
                s+=' '
                try:
                    if not np.isnan(sedf.loc[i,j]):
                        se='$(' + float_fmt % sedf.loc[i,j] + ')$' 
                        entry='| '+se+' '
                    else: entry='| '
                except KeyError: entry='| '
                s+=entry 
            s+='|\n'
        return s
def add_interactions(DF,varnames, overwrite=False):
    """
    Returns DF with specified interaction terms.
    varnames is a list of variable names connected by * or :
    The product of the two variables is added to DF
    if * -- adds the un-interacted variables to varnames
    if : -- changes it to * but does not add un-interacted variables
    returns DF *and* varnames
    """
    if type(varnames) not in (list, tuple): varnames=[varnames]
    #~ Crazy inefficient to do this?
    if not overwrite: DF = DF.copy()
    for v in varnames:
        if (v in DF) and (not overwrite): continue
        if "*" in v:
            i,j = v.split("*")
            for k in (i,j):
                if k not in varnames: varnames.append(k)
            DF[v] = DF[i]*DF[j]
        elif ":" in v:
            i,j = v.split(":")
            #~ varnames.remove(v)
            #~ v = i+"*"+j
            #~ varnames.append(v)
            DF[v] = DF[i]*DF[j]
        else: pass
    return DF, varnames
def regressions(DF,outcomes=[],controls=[], **kwargs):
    """ Runs a set of regressions and return a dict of {Outcome: sm.OLS (or RLM) model} for each model
     DF:
         The full dataset with outcomes and control variables.
     Year:
         A suffix on each outcome variable, specifying which round of data is being used. (Default to "")
     Baseline:
         A suffix on each variable to be used as a baseline covariate, specifying which round of data is being used.
         If the outcome variable doesn't have a corresponding column with that suffix, no baseline control is included and the function passes without error.
         (Default to "_bsln")
     Controls:
         A list or tuple of variables to be used as covariates in each regression.
     Outcomes:
         The list of outcomes (also the names of the models)
     rhs_extra: (Optional)
         A dictionary of covariates to be added to the regression for specific outcomes.
         So of the form {outcome: [list of controls]} for each outcome.
     Baseline_na:
         If True, code missing values of baseline variable as zero and include a "Bsln_NAN" indicator in Controls.
     Robust:
         If True, use statsmodel's RLM class instead of OLS (defaults to Huber-T se's)
     Cluster:
         If a variable name (a string) is given, clusters standard errors at that level
         If a dictionary of {outcome:level} is given, clusters standard errors at that level
            (applies statsmodels .get_robustcov_results())
     fe:
         If fe is a (non-empty) list
     drop_var:
         Statsmodels assumes a constant is present when prompted for a robust covariance matrix
         If a full set of indicators (say, year dummies) is supplied instead, doesn't know how to drop one like Stat does
         If supplied, drop_var manually specifies which variable gets dropped. (e.g. baseline year dummy, control group dummy, etc.)
     names:
         Alternative names for the models other than the name of the outcome variable
         Takes a dict with names as keys and outcomes as values.
         If not a dict, gets converted into one.
     Return:
         dict {outcome var:model} for each outcome in outcomes.
    """
    #~ Kwargs
    Year        = kwargs.setdefault("Year",  "")
    Baseline    = kwargs.setdefault("Baseline",  "_bsln")
    rhs_extra   = kwargs.setdefault("rhs_extra", {})
    baseline_na = kwargs.setdefault("baseline_na", True)
    robust      = kwargs.setdefault("robust",    False)
    interactions= kwargs.setdefault("interactions", True)
    cluster     = kwargs.setdefault("cluster", None)
    drop_var    = kwargs.setdefault("drop_var", None)
    names       = kwargs.setdefault("names", [])
    fe          = kwargs.setdefault("fe", [])
    
    if fe: regress = lambda y,X: fe_reg(y,X,group=fe)
    elif robust: regress=sm.RLM
    else: regress=sm.OLS
    if not type(Year)==str: Year=str(Year)
    if not type(Baseline)==str: Baseline=str(Baseline)

    models = {}
    if interactions: DF,controls = add_interactions(DF,controls)
    if cluster: #~ Can optionally provide a dict relating each outcome to a different clustering scheme.
        if type(cluster)!=dict: cluster={outcome:cluster for outcome in outcomes}

    for outcome in outcomes: #~ Run regressions and store models in a dictionary
        Xvars = controls

        #~ Add baseline value as control if present
        if outcome+Baseline in DF: #~ Present in DataFrame
            if DF[outcome+Baseline].count(): #~ Contains non-null values
                Xvars.append(outcome+Baseline)

        if outcome in rhs_extra: #~ If additional controls specified, make sure they're in there and add them
            if not type(rhs_extra[outcome]) in (list,tuple): rhs_extra[outcome] = [rhs_extra[outcome]] #~ Make into a list if not already.
            for x in rhs_extra[outcome]: #~ Check that it's present. (Warn user if not)
                try: assert(x in DF)
                except AssertionError: raise KeyError("Extra Covariate --{}-- for outcome --{}-- not found in data".format(x,outcome))
            if interactions: DF, rhs_extra[outcome] = add_interactions(DF, rhs_extra[outcome])
            Xvars += list(rhs_extra[outcome])
        othervars = [outcome+Year]
        if cluster and (not fe): othervars.append(cluster[outcome])
        
        df = DF[othervars +Xvars].rename(columns={outcome+Baseline:Baseline}) #~ if cluster: include cluster var in df

        #~ Include baseline==missing indicator as a control
        if Baseline in df and baseline_na:
            df["Bsln_NAN"] = df["Bsln"+Baseline].isnull().apply(int)
            df[Baseline].fillna(0,inplace=True)
            Xvars.append("Bsln_NAN")

        df = df.dropna()

        #~ Full-sample OLS
        models[outcome] = regress(df[outcome+Year], df[Xvars]).fit()
        if cluster:      #~ If specified, cluster standard errors
            if drop_var: #~ If specified, assign variable to be dropped
                drop_idx = df[Xvars].columns.tolist().index(drop_var)
                models[outcome].model.data.const_idx=drop_idx
            models[outcome] = models[outcome].get_robustcov_results(cov_type="cluster", groups=df[cluster[outcome]])
        del df, Xvars #~ remove from memory; just housekeeping, shouldn't be strictly necessary
    if names:
        if type(names)==str: names = {outcome+names:outcome for outcome in outcomes}
        if type(names) in (list,tuple): names=dict(zip(names,outcomes))
        for name in names.keys(): models[name]=models.pop(names[name])
        

    return models
def reg_table(models,**kwargs):
    """ Take a list or dict of sm.RegressionResults objects and create a nice table.
     Summary: (Default)
       If True, return a summary_col object (from sm.iolib.summary2), which allows for as_text and as_latex
     Orgtbl:
       If True, return an orgtable (uses df_to_orgtbl) for the OLS model params.
     Resultdf:
       Returns the coefficient and SE df's for modification and subsequent entry into df_to_orgtbl.
       Useful for adding other columns/rows, like control-group means
     table_info:
       A list of model statistics that can be included at the bottom (like with stata's esttab)
       Allows for "N", "R2", "R2-adj", "F-stat"
       Defaults to just "N"
     Transpose:
       Places outcomes on left with regressors on top.
    """

    summary    = kwargs.setdefault("summary",   True)
    orgtbl     = kwargs.setdefault("orgtbl",    False)
    resultdf   = kwargs.setdefault("resultdf",  False)
    table_info = kwargs.setdefault("table_info", "N")
    Transpose  = kwargs.setdefault("Transpose", False)
    summary    = not any((orgtbl, resultdf)) #~ Summary by default
 
    #~ Construct the Summary table, using either table or df_to_orgtbl
    if table_info:
        if type(table_info) not in (list,tuple): table_info=[table_info]
        info_dict = {"N": lambda model: model.nobs,
                     "R2": lambda model: model.rsquared,
                     "R2-adj": lambda model: model.rsquared_adj,
                     "F-stat": lambda model: model.fvalue}
        info_dict = dict([(x,info_dict[x]) for x in table_info])

    if summary:
        from statsmodels.iolib import summary2
        Summary = summary2.summary_col(list(models.values()), stars=True, float_format='%.3f',info_dict=info_dict)
        #~ This mangles much of the pretty left to the Summary2 object and returns a pd.DF w/o se's
        if Transpose: Summary = Summary.tables[0].T.drop("",1)

    else:
        # Extras = lambda model: pd.Series({"N":model.nobs})
        # results = pd.DataFrame({Var:model.params.append(Extras(model)) for Var,model in models.iteritems()})
        try:
            xrange
            Ms = lambda: models.iteritems()
        except NameError: Ms = lambda: models.items()
        results = pd.DataFrame({Var:model.params for Var,model in Ms()})
        SEs     = pd.DataFrame({Var:model.bse    for Var,model in Ms()})
        if table_info:
            try:
                info_dict.iteritems()
                info_items = lambda: info_dict.iteritems()
            except AttributeError: info_items = lambda: info_dict.items()
            extras = pd.DataFrame({Var: pd.Series({name:stat(model) for name,stat in info_items()}) for Var,model in Ms()})
            results = results.append(extras)
        if Transpose: results,SEs = results.T, SEs.T

        if orgtbl: Summary = df_to_orgtbl(results,sedf=SEs)
        else:
            assert(resultdf)
            Summary = results, SEs

    return Summary
def fe_reg(y,X,group=[], data=None, alert=True):
    """
    y is an outcome vector
    X is a matrix of controls
      UNLESS data is not None, where y and X are a string and list of strings

    `group' is a variable name in X to be used for the fixed-effects
    if data is not None, `group' will be added to X without warning or error if not included


    NOTES:

    There is *NO* note as to which FE's were used

    Demean-ing proceedure described in
    http://www.stata.com/support/faqs/statistics/intercept-in-fixed-effects-model/

    The standard errors *still* aren't quite the same, but closer
    but point estimates appear to be correct
    Even if data is missing
    """
    if data is not None:
        if group not in X: X.append(group)
        y, X = data[y], data[X]
    try: y.name #~ Check that y is a Series
    except AttributeError: y=pd.Series(y.iloc[:,0])
    if not y.name: Outcome="Y" #~ If y doesn't have a name, call it "Y"
    else: Outcome=y.name
    #~ Adding Outcome to X for the demeaning and everythign
    X[Outcome] = y
    X = X.dropna()
    VARS = [var for var in X.columns if var is not group]

    #~ def demean(df):
    #~     M = df.mean()
    #~     for var in df:
    #~         if var in M: df[var] = df[var]-M[var]
    #~     return df

    GrandMean = X[VARS].mean()
    if alert: print("De-meaninging")
    demean = lambda grp: grp-grp.mean()
    X = X.groupby(group)[VARS].apply(demean)
    #~ X = pd.concat([demean(grp_data.drop(group,1)) for grp, grp_data in byGroup])
    for var in VARS:
        if var in GrandMean: X[var] = X[var]+GrandMean[var]
    X['const'] = 1.
    y, X = X[Outcome], X.drop(Outcome, 1)
    if alert: print("Running Regression")
    model = sm.OLS(y,X,missing='drop')
    return model
