
resultsdir='C:\\Users\\Megan\\ghana2\\UCB\\ML_SYP\\Code\\Expenditures - Copy\\Results\\'
BOOTSTRAP=0
# -*- coding: utf-8 -*-
import os 
os.chdir('C:\\Users\\Megan\\ghana2\\UCB\ML_SYP\\Code\\Expenditures - Copy')
import numpy as np
import pandas as pd
from numpy.linalg import norm
import sys
#sys.path.append('../Empirics')
import neediness as nd
from ghana_data import y,z

#use_goods=y.columns[y.count()>1000]
use=y.columns[y.count()>500]
y=y[use]

                
# Define market here, or set to None for a single market
market=None #mydf['Rural']

# Define numeraire good here!  If None, use natural price index
#ML: I'll use maize/corn bowl for now
numeraire = 'Maize/Corn' 

#ML: I'm commening out all of the below because I am doing everything in quantities for now
# Also give prices for good to be declared numeraire.
# E.g., here are median rice prices and onion prices in different years from LSMS/Uganda/MANIFEST.org:
#price_index=pd.Series({2005:1200,2009:2000,2010:2400,2011:3000},name='Rice')
#price_index.index.name='t'

#otherprice=pd.Series({2005:100,2009:200,2010:300,2011:500},name='Onions')/price_index
#otherprice.index.name='t'

# y[numeraire]=y[numeraire].sub(np.log(price_index)) # Now this is hopefully roughly quantities!
#foo,bar=y.align(np.log(price_index),level='t',axis=0)
#y=y.sub(bar,axis=0) # Now expenditures in terms of numeraire

try:
    using_goods=(y[use_goods].T.count()>=np.floor(len(use_goods)/3.))
    y=y.loc[using_goods,use_goods] # Drop households with too few expenditure observations, keep selected goods
except NameError:
    y=y.loc[y.T.count()>10,:] # Drop households with too few expenditure observations

b,ce,d,se,V=nd.estimate_bdce_with_missing_values(y,z,market=market,prices=None,return_se=True,return_v=True)
ce.dropna(how='all',inplace=True)
ce.to_pickle(resultsdir + 'ce.df')

from oct2py import Oct2Py
octave=Oct2Py()
octave.addpath('C:\\Users\\Megan\\vesdemand\\utils\\IncPACK')

bphi,logL=nd.get_loglambdas(ce,TEST=True)

cehat=np.outer(pd.DataFrame(bphi),pd.DataFrame(-logL).T).T
cehat=pd.DataFrame(cehat,columns=bphi.index,index=logL.index)
cehat.to_pickle(resultsdir+'cehat.df')
V.to_pickle(resultsdir+'d_covariance.df')

print "Norm of error in approximation of CE divided by norm of CE: %f" % (nd.df_norm(cehat,ce)/nd.df_norm(ce))

# Some naive standard errors & ANOVA
anova=pd.DataFrame({'Prices':b.T.var(ddof=0),'Characteristics':z.dot(d.T).var(ddof=0),'$\log\lambda$':cehat.var(ddof=0),'Residual':(ce-cehat).var(ddof=0)})
anova=anova.div(y.var(ddof=0),axis=0)
anova['Total var']=y.var(ddof=0)
anova.sort_values(by=r'$\log\lambda$',inplace=True)

anova.to_pickle(resultsdir + 'anova.df')

yhat=b.T.add(cehat + (z.dot(d.T)),axis=0,level='t')
yhat0=b.T.add(ce + (z.dot(d.T)),axis=0,level='t')

e=y.sub(yhat) #This residual includes the log lambdas, the log sums, and the classical error term

if numeraire is not None:
    print 'CURR NUM: %s\n\n' % numeraire
    #raw_input()
    barloglambda_t=-b.loc[numeraire]/bphi[numeraire]
    logL=logL.add(barloglambda_t,axis=0,level='t') # Add term associated with numeraire good
    b=b-pd.DataFrame(np.outer(bphi,barloglambda_t),index=bphi.index,columns=barloglambda_t.index)
    logalpha=b['baseline'].T
    prices=np.exp((b.T-b['baseline']).div((1-bphi),axis=0))
else:
    pidx=b.mean()
    logL=logL.add(pidx,axis=0,level='t') # Add term associated with numeraire good
    b=b-pidx

print "Mean of errors:"
print e.mean(axis=0)

goodsdf=d.copy()
goodsdf[r'$\phi\beta_i$']=bphi
goodsdf['$R^2$']=1-e.var()/y.var()

ehat=e.dropna(how='all')
ehat=ehat-ehat.mean() #Why is this the new error term after we have calculated log lambdas?

if BOOTSTRAP:
    print "Bootstrapping..."
    sel,Bs=nd.bootstrap_elasticity_stderrs(ce,VERBOSE=False,return_samples=True)
    Bs.to_pickle(resultsdir+"phibeta_bootstrap_sample.df")
    se[r'$\phi\beta_i$']=sel
else:
    try:
        print "Recovering bootstrapped sample..."
        Bs=pd.read_pickle(resultsdir+"phibeta_bootstrap_sample.df")
        se[r'$\phi\beta_i$']=Bs.std()
    except IOError:
        sel=[]
        for i in ehat:
            foo=pd.DataFrame({'logL':logL,'e':ehat[i]}).dropna(how='any')
            sel.append(np.sqrt(nd.arellano_robust_cov(foo['logL'],foo['e'])))
        se[r'$\phi\beta_i$']=sel
  
goodsdf=goodsdf[[r'$\phi\beta_i$']+d.columns.tolist()+['$R^2$']]

goodsdf['%Zero']=100-np.round(100*(~np.isnan(y[goodsdf.index])+0.).mean(),1)
goodsdf['logalpha']=logalpha
se['logalpha']=ehat.query('t==0').std()/np.sqrt(ehat.query('t==0').count())

logL=logL.unstack()

goodsdf.sort_values(by=[r'$\phi\beta_i$'],inplace=True,ascending=False)
goodsdf.to_pickle(resultsdir+'goods.df')
se.to_pickle(resultsdir+'stderrs.df')
b.to_pickle(resultsdir+'a_it.df')
logL.to_pickle(resultsdir+'loglambda.df')
prices.to_pickle(resultsdir+'prices.df')
y.to_pickle(resultsdir+'logexpenditures.df')
yhat.to_pickle(resultsdir+'logexpenditures_hat.df')
yhat0.to_pickle(resultsdir+'logexpenditures_hat0.df')

pd.concat([logL.stack(),z],axis=1).rename(columns={0:'$\log\lambda$'}).to_pickle(resultsdir+'hh.df')

#######################Now do the analysis for the household table##########################
hh=['Boys','Girls','Men','Women']
z=pd.read_pickle(resultsdir+"hh.df")[hh]

d=pd.read_pickle(resultsdir+"goods.df")[hh+['log HSize']]
dse=pd.read_pickle(resultsdir+"stderrs.df")[hh+['log HSize']]
dV=pd.read_pickle(resultsdir+"d_covariance.df")

d0=d[['log HSize']]
del d['log HSize'] #Now d is a df with the estimated coefficients on boy, girls, etc. from the
                   #previous analysis.

d0se=dse['log HSize']
del dse['log HSize'] #Now dse is a df with the estimated SEs on boy, girls, etc. from the
                   #previous analysis.

#s divides z so that now boys+girls+men+women=1. First, I'll take out rows where the sum
#of household members is 0
z['Total'] = z.sum(axis=1)
z = z[z.Total != 0]
del z['Total']
s=z.div(z.sum(axis=1),axis=0) 

b=np.outer(d0,s.mean()) #This is taking the outer product of a vector of HHsize (36x1) and 
                        #a vector of average hhcomposition percentages (4x1)

a=d.mul(z.mean(),axis=1) #a is a df that comes from multiplying d by a 4x1 vector of average
                         #numbers of boy,girls,etc. (this is levels, NOT percentages)
                         
e=(a + b) #I'm a litte confused about the reasoning here, although I understand that it is 
          #creating the esitmates for the elasticities evaluated at the average household 
          #composition.

SE=[]
for i in d.index: # Iterate over goods
    v = dV[i].loc[hh,hh]*z.var() 
    v += 2 * (z.as_matrix().T.dot(s)/z.shape[0] - np.outer(z.mean(),s.mean()))*dV[i].loc[hh,['log HSize']].as_matrix()
    v += dV[i].loc['log HSize','log HSize']*s.var()
    SE.append(np.sqrt(np.diag(v)))

SE=pd.DataFrame(SE,index=d.index,columns=d.columns)

#print df_to_orgtbl(e,sedf=SE,tdf=e/SE,float_fmt='%4.2f')

