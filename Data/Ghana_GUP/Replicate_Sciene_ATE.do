********************************************************
*Effects of Ghana GUP on total consumption - Replication
*Author: Megan Lang
*Date: 4/14/2017
********************************************************

/****************************************************************************
Using aggregates already in the data
****************************************************************************/
clear all 

use "/Users/meganlang/ghana2/Data/analysis/panel/working/panel_hh.dta"
keep FPrimary round numvill village_status household_status hhsize hhsize_h ///
consumption_food_cap Cnonfood_month_cap Cdurable_month_cap

*Save baseline data to merge back in later
keep if round==0
gen food_bl = consumption_food_cap
gen nonfood_bl = Cnonfood_month_cap
gen durables_bl = Cdurable_month_cap
keep FPrimary food_bl nonfood_bl durables_bl
saveold baseline.dta, replace 

use "/Users/meganlang/ghana2/Data/analysis/panel/working/panel_hh.dta"
keep FPrimary round numvill village_status household_status hhsize hhsize_h ///
Cnonfood_month_cap Cdurable_month_cap ///
consumption_food_cap 

*Merge baseline values back in as variables
merge m:1 FPrimary using baseline.dta

*Account for household size 
*gen food_pc = consumption_food_cap

*Account for exchange rate (USD PPP, given in endnote of Science article)
gen food_USD = consumption_food_cap/0.79 if round==4
replace food_USD = consumption_food_cap/0.91 if round==6

/*Adjust for inflation to Dec. 2014 levels given in endnote of Science paper*/
gen food = food_USD*(236.9111/232.957) if (round==4)

*Generate relevant treatment variable
gen GUP = 0
replace GUP = 1 if (village_status==1 & household_status!=3)
*gen SOUP = 0
*replace SOUP = 1 if (village_status==2 & household_status!=6)
*gen assets = 0
*replace assets = 1 if (village_status==3 & household_status!=8)
keep if (village_status==0|village_status==1)

*Generate cluster variable - cluster by village for control only
gen clusterby = FPrimary
replace clusterby = numvill if village_status==0
encode numvill, gen(geo_cluster)

*Two year ITT
areg food GUP food_bl nonfood_bl durables_bl if (round==4), absorb(geo_cluster) cluster(clusterby)

*Three year ITT
areg food_USD GUP food_bl nonfood_bl durables_bl if (round==6), absorb(geo_cluster) cluster(clusterby)



