#! /usr/bin/env python
import numpy as np
import pandas as pd
import cfe.estimation as nd
import statsmodels.api as sm
def winsorize(Series, **kwargs):
    """
    Need to implement two-sided censoring as well.
    WARNING: if Top<0, all zeros will be changed to Top
    """

    percent    = kwargs.setdefault("percent",99)
    stdev      = kwargs.setdefault("stdev",False)
    drop       = kwargs.setdefault("drop",False)
    drop_zeros = kwargs.setdefault("drop_zeros",True)
    twoway     = kwargs.setdefault("twoway",False)

    if drop_zeros: S = Series.replace(0,np.nan).dropna()
    else: S = Series.dropna()
    N_OBS = S.notnull().sum()
    if N_OBS<10: return S

    if percent: Top = np.percentile(S, percent)
    if stdev:   
        Top =  S.dropna().mean()
        Top += stdev*S.dropna().std()
    try: assert((not drop_zeros) or Top>0)
    except AssertionError: raise ValueError("Top < 0 but zeros excluded")
    if drop: replace_with = np.nan
    else:    replace_with = Top
    Series[Series>Top]=replace_with

    if not twoway: return Series
    else:
        kwargs['twoway'] = False
        return -1*topcode(-1*Series, **kwargs)
def group_expenditures(df,groups):
    X=pd.DataFrame(index=df.index)
    for k,v in groups.iteritems():
        X[k]=df[v].sum(axis=1)
            
    return X
def bangladesh_yz(File="Bangladesh_TUP.hdf",DIR="./", WRITE=True,aggregate = False, topcode=False, use_logs=True, restrict=False, location_id= "region"):
    """
    Read in bangladesh dataset and returns y & z ready for use in the cfe module
    """
    File = DIR+File
    #~ Read in dataset
    D = pd.read_hdf(File, key="Bangladesh_TUP")
 
    #~ Define Consumption Items and Household Features
    Items = D.columns.tolist()[5:]
    hh_features= ["Girls", "Boys", "Men", "Women","log HHSIZE"]

    if restrict:
        D['Adults'] =   D[['Men','Women']].sum(axis=1)
        D['Children'] = D[['Boys','Girls']].sum(axis=1)
        hh_features = ['Adults','Children','log HHSIZE']

    z = D[hh_features]
    y = D[Items]    

    if aggregate:
        groups = {}
        for row in filter(lambda i: i[1], codes):
          k, v = row[0], row[1]
          if k in groups.keys():
            groups[k] = groups[k] + [v]
          else:
            groups[k] = [v]
        y = group_expenditures(y, groups).replace(0, np.nan)

    if use_logs: y = np.log(y.replace(to_replace=0.,value=np.NaN))
    if WRITE:
        y.to_csv("expenditures.csv")
        z.to_csv("hh.csv")
  
    return y,z

resultsdir = "../../Results/Bangladesh_TUP/"
numeraire  = 'Onion'   
BOOTSTRAP  = True


try:
    z = pd.read_csv("hh.csv").set_index(['j','t','mkt'])
    y = pd.read_csv("expenditures.csv").set_index(['j','t','mkt'])
    if max(y['Rice'])>10: y = y.replace(0,np.nan).apply(np.log)  #~ Check whether expenditures have been saved in levels (which they should be) before taking logs.
except EnvironmentError: y,z = bangladesh_yz() #~ Read in from provided hdf if csv's not present.

b,ce,d,se,V = nd.estimate_reduced_form(y,z,return_se=True,return_v=True,VERBOSE=True)
print("Getting Loglambdas")
bphi,logL=nd.get_loglambdas(ce,TEST="warn")

cehat=np.outer(pd.DataFrame(bphi),pd.DataFrame(-logL).T).T
cehat=pd.DataFrame(cehat,columns=bphi.index,index=logL.index)

print("Norm of error in approximation of CE divided by norm of CE: %f" % (nd.df_norm(cehat,ce)/nd.df_norm(ce)))

# Some naive standard errors & ANOVA
anova=pd.DataFrame({'Prices':b.T.var(ddof=0),'Characteristics':z.dot(d.T).var(ddof=0),'$\log\lambda$':cehat.var(ddof=0),'Residual':(ce-cehat).var(ddof=0)})
anova=anova.div(y.var(ddof=0),axis=0)
anova['Total var']=y.var(ddof=0)
try: anova.sort_values(by=r'$\log\lambda$',inplace=True)
except AttributeError: anova.sort(columns=r'$\log\lambda$',inplace=True) 
anova.to_pickle(resultsdir+'anova.df')

yhat = nd.broadcast_binary_op(cehat+z.dot(d.T), lambda x,y: x+y, b.T)
yhat0= nd.broadcast_binary_op(ce   +z.dot(d.T), lambda x,y: x+y, b.T)

e=y.sub(yhat) #This residual includes the log lambdas, the log sums, and the classical error term

firstyear = y.index.get_level_values('t').min()

if numeraire is not None:
    print('CURR NUM: %s\n\n' % numeraire)
    #raw_input()
    barloglambda_t=-b.loc[numeraire]/bphi[numeraire]
    #logL=logL.add(barloglambda_t,axis=0,level='t') # Add term associated with numeraire good
    logLmean0 = logL.copy()
    logL = nd.broadcast_binary_op(logL,lambda x,y:x+y, barloglambda_t)
    b=b-pd.DataFrame(np.outer(bphi,barloglambda_t),index=bphi.index,columns=barloglambda_t.index)
    logalpha=b[firstyear].T
    prices=np.exp((b-b[firstyear]).div((1-bphi),axis=0))
else:
    pidx=b.mean()
    logL=logL.add(pidx,axis=0,level='t') # Add term associated with numeraire good
    b=b-pidx

print("Mean of errors:")
print(e.mean(axis=0))

goodsdf=d.copy()
goodsdf[r'$\phi\beta_i$']=bphi
goodsdf['$R^2$']=1-e.var()/y.var()

ehat=e.dropna(how='all')
ehat=ehat-ehat.mean() #Why is this the new error term after we have calculated log lambdas?

if BOOTSTRAP:
    print("Bootstrapping...")
    sel,Bs=nd.bootstrap_elasticity_stderrs(ce,VERBOSE=False,return_samples=True)
    Bs.to_pickle(resultsdir+"phibeta_bootstrap_sample.df")
    se[r'$\phi\beta_i$']=sel
else:
    try:
        print("Recovering bootstrapped sample...")
        Bs=pd.read_pickle(resultsdir+"phibeta_bootstrap_sample.df")
        se[r'$\phi\beta_i$']=Bs.std()
    except IOError:
        sel=[]
        for i in ehat:
            foo=pd.DataFrame({'logL':logL,'e':ehat[i]}).dropna(how='any')
            sel.append(np.sqrt(nd.arellano_robust_cov(foo['logL'],foo['e'])))
        se[r'$\phi\beta_i$']=sel
 

goodsdf=goodsdf[[r'$\phi\beta_i$']+d.columns.tolist()+['$R^2$']]
try: goodsdf['constant']=constant
except NameError: pass

goodsdf['%Zero']=100-np.round(100*(~np.isnan(y[goodsdf.index])+0.).mean(),1)
goodsdf['logalpha']=logalpha.mean(0)  #~  NOTE THAT THIS NEEDS FIXING. THIS IS NOT HOW TO DO THIS.
se['logalpha']=ehat.query('t=='+str(2013)).std()/np.sqrt(ehat.query('t=='+str(2013)).count())

if WRITE_RESULTS:
    try: goodsdf.sort_values(by=[r'$\phi\beta_i$'],inplace=True,ascending=False)
    except AttributeError: goodsdf.sort(r'$\phi\beta_i$',inplace=True,ascending=False)
    goodsdf[r'$\phi\beta_i$'].to_pickle(resultsdir+"betas.df")
    goodsdf.to_pickle(resultsdir+'goods.df')
    b.to_pickle(resultsdir+'a_it.df')
    logL.to_pickle(resultsdir+'loglambdas.df')
    prices.to_pickle(resultsdir+'prices.df')
    y.to_pickle(resultsdir+'logexpenditures.df')
    yhat.to_pickle(resultsdir+'logexpenditures_hat.df')
    #~ yhat0.to_pickle(resultsdir+'logexpenditures_hat0.df')
    pd.concat([logL,z],axis=1).rename(columns={0:'$\log\lambda$'}).to_pickle(resultsdir+'hh.df')
    se.to_pickle(resultsdir+'stderrs.df')


