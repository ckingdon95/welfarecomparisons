#! /usr/bin/env python
import numpy as np
import pandas as pd

D = pd.read_csv("TUP_full.csv")
t = D[['idno','TUP','Cash']].rename(columns={'idno':'HH'}).set_index("HH").fillna(0)
t['Control'] = 1-t[['Cash','TUP']].any(1)

T = pd.read_pickle("ss-hh.df").join(t)[['TUP',"Control",'Cash']].fillna(0)
T = T.reorder_levels([1,0,2])
#~ Manually set TUP to trigger in 2014 and Cash in 2015
T.loc[2013,['Control']]    = 1.
T.loc[2013,['TUP','Cash']] = 0.

T.loc[2014,['Cash']]       = 0
T.loc[2014,['Control']]    = (1-T.loc[2014,['TUP','Cash']].any(1)).values
T = T.reorder_levels([1,0,2])
T.index.names=['j','t','mkt']
T.to_csv("ss-treatment.csv")
T.to_hdf("ss-treatment.hdf",key="treatment")

