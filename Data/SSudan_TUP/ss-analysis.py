#! /usr/bin/env python
import numpy as np
import pandas as pd
import cfe.estimation as nd
import statsmodels.api as sm
from TUP import full_data, consumption_data, regressions, reg_table, df_to_orgtbl, mobile_data
food =  ['cereals', 'maize', 'sorghum', 'millet', 'sweetpotato', 'rice', 'bread', 'beans', 'oil', 'salt', 'sugar', 'meat', 'livestock', 'poultry', 'fish', 'egg', 'nuts', 'milk', 'vegetables', 'fruit', 'tea', 'spices', 'alcohol', 'otherfood'] # 'potato', 
month = ['fuel', 'medicine', 'airtime', 'cosmetics', 'soap', 'transport', 'entertainment', 'childcare', 'tobacco', 'batteries', 'church', 'othermonth']    
ConsumptionItems = food+['airtime','fuel']
get_mobile_values = False
resultsdir = "../../Results/SSudan/ss-"
numeraire  = 'cereals'
BOOTSTRAP  = True

try:
    C,HH,Treat = [pd.read_pickle(F) for F in ("ss-consumption.df","ss-hh.df","ss-treatment.df")]
except: 
    D = full_data(File="TUP_full.csv")
    hhvars = "men women girls boys".split()
    C, HH, Treat = consumption_data(D,hh_vars=hhvars,WRITE="csv",include2016=False)
    HH['women'].replace(0,1,inplace=True) #~ Respondents are all female household members
    HH['lnSize'] = HH.sum(1).replace(0,1).apply(np.log)
    hhvars = HH.columns.tolist()

y,z = C.replace(0,np.nan).apply(np.log).sort_index(level=[0,1,2])[ConsumptionItems].copy(),HH.sort_index(level=[0,1,2]).copy()
y.index.names, z.index.names = ['j','t','mkt'], ['j','t','mkt']
keep = pd.notnull(y.index.get_level_values("mkt"))
y,z = y.loc[keep,:].align(z,join="left",axis=0)
b,ce,d,sed,V = nd.estimate_reduced_form(y,z,return_se=True, return_v=True,VERBOSE=True)
ce = ce.dropna(how='all')
print("Getting Loglambdas")
bphi,logL=nd.get_loglambdas(ce,TEST="warn")

if get_mobile_values:
    M,Mc,Mhh = mobile_data(use_dates=True)
    y = Mc.replace(0,np.nan).apply(np.log).sort_index(level=[0,1,2]).filter(items=ConsumptionItems).copy()
    z = Mhh.sort_index(level=[0,1,2]).copy()
    y.index.names, z.index.names = ['j','t','mkt'], ['j','t','mkt']
    keep = pd.notnull(y.index.get_level_values("mkt"))
    y,z = y.loc[keep,:].align(z,join="left",axis=0)
    b,ce,d,sed,V = nd.estimate_reduced_form(y,z,return_se=True, return_v=True,VERBOSE=True)
    ce = ce.dropna(how='all')
    print("Getting Loglambdas")
    Mbphi,MlogL=nd.get_loglambdas(ce,TEST="warn")
    C,HH = Mc,Mhh

cehat=np.outer(pd.DataFrame(bphi),pd.DataFrame(-logL).T).T
cehat=pd.DataFrame(cehat,columns=bphi.index,index=logL.index)
cehat.to_pickle(resultsdir+'cehat.df')
V.to_pickle(resultsdir+'d_covariance.df')

print("Norm of error in approximation of CE divided by norm of CE: %f" % (nd.df_norm(cehat,ce)/nd.df_norm(ce)))

# Some naive standard errors & ANOVA
anova=pd.DataFrame({'Prices':b.T.var(ddof=0),'Characteristics':z.dot(d.T).var(ddof=0),'$\log\lambda$':cehat.var(ddof=0),'Residual':(ce-cehat).var(ddof=0)})
anova=anova.div(y.var(ddof=0),axis=0)
anova['Total var']=y.var(ddof=0)
anova.sort_values(by=r'$\log\lambda$',inplace=True)

anova.to_pickle(resultsdir + 'anova.df')

yhat = nd.broadcast_binary_op(cehat+z.dot(d.T), lambda x,y: x+y, b.T)
yhat0= nd.broadcast_binary_op(ce   +z.dot(d.T), lambda x,y: x+y, b.T)


e=y.sub(yhat) #This residual includes the log lambdas, the log sums, and the classical error term

firstyear = y.index.get_level_values('t').min()

if numeraire is not None:
    print('CURR NUM: %s\n\n' % numeraire)
    #raw_input()
    barloglambda_t=-b.loc[numeraire]/bphi[numeraire]
    #logL=logL.add(barloglambda_t,axis=0,level='t') # Add term associated with numeraire good
    logLmean0 = logL.copy()
    logL = nd.broadcast_binary_op(logL,lambda x,y:x+y, barloglambda_t)
    b=b-pd.DataFrame(np.outer(bphi,barloglambda_t),index=bphi.index,columns=barloglambda_t.index)
    logalpha=b[firstyear].T
    prices=np.exp((b-b[firstyear]).div((1-bphi),axis=0))
else:
    pidx=b.mean()
    logL=logL.add(pidx,axis=0,level='t') # Add term associated with numeraire good
    b=b-pidx

print("Mean of errors:")
print(e.mean(axis=0))

goodsdf=d.copy()
goodsdf[r'$\phi\beta_i$']=bphi
goodsdf['$R^2$']=1-e.var()/y.var()

ehat=e.dropna(how='all')
ehat=ehat-ehat.mean() #Why is this the new error term after we have calculated log lambdas?

if BOOTSTRAP:
    print("Bootstrapping...")
    sel,Bs=nd.bootstrap_elasticity_stderrs(ce,VERBOSE=False,return_samples=True)
    Bs.to_pickle(resultsdir+"phibeta_bootstrap_sample.df")
    se[r'$\phi\beta_i$']=sel
else:
    try:
        print("Recovering bootstrapped sample...")
        Bs=pd.read_pickle(resultsdir+"phibeta_bootstrap_sample.df")
        se[r'$\phi\beta_i$']=Bs.std()
    except IOError:
        sel=[]
        for i in ehat:
            foo=pd.DataFrame({'logL':logL,'e':ehat[i]}).dropna(how='any')
            sel.append(np.sqrt(nd.arellano_robust_cov(foo['logL'],foo['e'])))
        se[r'$\phi\beta_i$']=sel
 

goodsdf=goodsdf[[r'$\phi\beta_i$']+d.columns.tolist()+['$R^2$']]
try: goodsdf['constant']=constant
except NameError: pass

goodsdf['%Zero']=100-np.round(100*(~np.isnan(y[goodsdf.index])+0.).mean(),1)
goodsdf['logalpha']=logalpha.mean(0)  #~  NOTE THAT THIS NEEDS FIXING. THIS IS NOT HOW TO DO THIS.
se['logalpha']=ehat.query('t=='+str(2013)).std()/np.sqrt(ehat.query('t=='+str(2013)).count())

if WRITE_RESULTS:
    try: goodsdf.sort_values(by=[r'$\phi\beta_i$'],inplace=True,ascending=False)
    except AttributeError: goodsdf.sort(r'$\phi\beta_i$',inplace=True,ascending=False)
    goodsdf[r'$\phi\beta_i$'].to_pickle(resultsdir+"betas.df")
    goodsdf.to_pickle(resultsdir+'goods.df')
    b.to_pickle(resultsdir+'a_it.df')
    logL.to_pickle(resultsdir+'loglambdas.df')
    prices.to_pickle(resultsdir+'prices.df')
    y.to_pickle(resultsdir+'logexpenditures.df')
    yhat.to_pickle(resultsdir+'logexpenditures_hat.df')
    #~ yhat0.to_pickle(resultsdir+'logexpenditures_hat0.df')
    pd.concat([logL,z],axis=1).rename(columns={0:'$\log\lambda$'}).to_pickle(resultsdir+'hh.df')
    se.to_pickle(resultsdir+'stderrs.df')



