% Created 2017-06-28 Wed 10:03
\documentclass[t,presentation]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{marvosym}
\usepackage{wasysym}
\usepackage{amssymb}
\usepackage{hyperref}
\tolerance=1000
\newcommand{\T}{\top}
\newcommand{\E}{\ensuremath{\mbox{E}}}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\Eq}[1]{(\ref{eq:#1})}
\newtheorem{proposition}{Proposition} \newcommand{\Prop}[1]{Proposition \ref{prop:#1}}
\newcommand{\Fig}[1]{Figure \ref{fig:#1}} \newcommand{\Tab}[1]{Table \ref{tab:#1}}
\usetheme{Madrid}
\usecolortheme{}
\usefonttheme{}
\useinnertheme{}
\useoutertheme{}
\author{}
\date{\today}
\title{Comparing Welfare Across Surveys}
\hypersetup{
  pdfkeywords={},
  pdfsubject={},
  pdfcreator={Emacs 24.5.1 (Org mode 8.2.10)}}
\begin{document}

\maketitle

\section*{The Big Idea}
\label{sec-1}

\begin{frame}[label=sec-1-1]{The Big Idea}
Are different surveys measuring the ``same thing''?

\begin{itemize}
\item Whole bodies of research may seek to change a particular outcome
\item but measure with different instruments \& contexts
\begin{itemize}
\item Does \alert{income} include interest payments?
\item Does tending a garden make someone employed?
\item Does \$2/day include last year's funeral?
\end{itemize}
\item Interpretation depends on these details
\end{itemize}

\alert{Economic theory can lend generality/comparability to otherwise inconsistent measurements.}
\end{frame}

\begin{frame}[label=sec-1-2]{Eight Experiments}
\begin{itemize}
\item Same basic topic: TUP/Graduation Programs
\item Same outcome of interest: Aggregate Consumption
\item Similar time frames
\item Very different consumption modules\ldots{}
\end{itemize}

Demand theory shows us how to aim at the same underlying parameter despite disparate
datasets.
\end{frame}

\begin{frame}[label=sec-1-3]{A Standard Approach}
\begin{enumerate}
\item Collect a basically ``complete'' expenditure panel
\item Aggregate goods, divide by HH size, do PPP adjustment
\end{enumerate}

Practical issues:

\begin{itemize}
\item Varying basket composition, recall window
\item PPP is tricky and expensive to do right.
\item Goods w/ high budget shares can be highly mismeasured.
\end{itemize}
\end{frame}

\begin{frame}[label=sec-1-4]{A Theoretical Issue}
\begin{itemize}
\item An incomplete set of goods has a different income elasticity from the full set.
\end{itemize}


\begin{itemize}
\item Engel curves aren't linear, especailly in
\end{itemize}


\begin{itemize}
\item Vary by income, depend on price of each good
\end{itemize}
\end{frame}


\section*{Our Approach}
\label{sec-2}

\begin{frame}[label=sec-2-1]{Our Approach}
\begin{itemize}
\item Estimate a (Frisch) demand system

\begin{itemize}
\item Back out $\lambda$$_{\text{it}}$, marginal utility in consumption

\item Eases up on empirical requirements

\item Allows for non-homothetic preferences
\end{itemize}

\item We try to place very different surveys in the same parameter space

\item Then measure relative size of treatment effects
\end{itemize}
\end{frame}

\begin{frame}[label=sec-2-2]{Starting with three experiments}
\begin{description}
\item[{Ghana}] Udry et al. (forthcoming); Banerjee et al. (2015)
\end{description}


\begin{description}
\item[{Bangladesh}] Bandiera et al. (2015)
\end{description}


\begin{description}
\item[{South Sudan}] Collins, Ligon, Sulaiman, Chowdhury (2015)
\end{description}
\end{frame}


\begin{frame}[label=sec-2-3]{Aggregate Consumption \& (a little) replication}
\begin{center}
\begin{tabular}{lllllr}
\hline
ATE (USD PPP) & Midline & $\leftarrow$ Repl. & Endline & $\leftarrow$ Repl. & Obs.\\
\hline
Bangladesh & 30.19 & -17.72 & 62.62 & \$84.01$^{\text{**}}$ & 18838\\
 & (25.34) & (9.01) & (20.82) & (12.18) & \\
Ghana & 2.18 & 2.56 & 2.41 & 2.72 & 2525\\
 & (1.04) & (1.32) & (0.81) & (0.99) & \\
South Sudan & 7.90 & ---- & 1.470 & ---- & 675\\
 & (2.73) &  & (2.15) &  & \\
\hline
\end{tabular}
\end{center}

(note: not just for ``meta-analysis''. Worthwhile for original research)
\end{frame}

\begin{frame}[label=sec-2-4]{Method}
\begin{itemize}
\item Using disaggregated consumption panels

\begin{itemize}
\item We can estimate $\lambda$$_{\text{it}}$ with a common model across all three surveys
\end{itemize}

\item Need to scale/deflate units of $\lambda$$_{\text{it}}$ with a price index

\begin{itemize}
\item Only need a common numeraire across each panel
\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}[label=sec-2-5]{A Frisch Demand System}
We describe a HH w/ features z$_{\text{i}}$ consuming goods c$_{\text{j}}$ with MU structure

\begin{equation*}
  \log \frac{\partial u(C,z)}{c_{j}} = \alpha(z) + \Gamma C + \epsilon_{j}
\end{equation*}

Taking X as the j-vector of expenditures for HH \emph{i}, Ligon (2016) shows this implies
the expenditure system

\begin{equation*}
  \log X_{i} = \alpha z_{i}  - \Theta \log P - \beta \log \lambda_{i} + e_{j}
\end{equation*}
\end{frame}

\begin{frame}[label=sec-2-6]{Deriving $\beta$$_{\text{j}}$}
\begin{equation*}
  \log X_{i} = \alpha z_{i}  - \Theta \log P - \beta \log \lambda_{i} + e_{j}
\end{equation*}

\begin{itemize}
\item Need to find a basket in each sample \emph{s} where $\beta$$^{\text{s}}$ $\approx$ $\phi$$\beta$$^{\text{0}}$

\begin{itemize}
\item The resulting baskets basically \emph{do} have the same (non-linear) Engel curves

\item (doesn't \emph{have} to be the same goods, either)
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[label=sec-2-7]{Deriving $\log$$\lambda$$_{\text{it}}$ and prices}
\begin{itemize}
\item The corresponding vector of $\log$$\lambda$'s are proportional across samples
\end{itemize}


\begin{itemize}
\item Need prices for at least one good in all three samples (Rice)
\end{itemize}


\begin{itemize}
\item Deflate the derived $\log$$\lambda$$_{\text{it}}$ into common numeraire units
\end{itemize}
\end{frame}


\section*{Results}
\label{sec-3}

\begin{frame}[label=sec-3-1]{Results: Elasticity Estimates}
\begin{table}[htb]
\caption{\label{tab:beta_comparison}Comparison of estimated Frisch elasticities, identified by setting the standard deviation of $\log$$\lambda$ to unity.}
\centering
\begin{tabular}{lllll}
 & Pooled & BGD & GHA & SSD\\
\hline
Beans & $0.530^{***}$ & $0.527^{***}$ & $1.055^{**}$ & $0.381^{***}$\\
 & $(0.058)$ & $(0.089)$ & $(0.517)$ & $(0.082)$\\
Meat & $0.885^{***}$ & $1.009^{***}$ & $-1.902^{*}$ & $0.385^{**}$\\
 & $(0.230)$ & $(0.233)$ & $(1.104)$ & $(0.183)$\\
Oil & $0.507^{***}$ & $0.465^{***}$ & $0.443$ & $0.928^{***}$\\
 & $(0.029)$ & $(0.027)$ & $(0.284)$ & $(0.063)$\\
Sugar & $0.638^{***}$ & $0.767^{***}$ & $0.292$ & $0.819^{***}$\\
 & $(0.055)$ & $(0.088)$ & $(0.216)$ & $(0.072)$\\
Vegetables & $0.903^{***}$ & $0.928^{***}$ & $0.985^{***}$ & $0.812^{***}$\\
 & $(0.020)$ & $(0.026)$ & $(0.068)$ & $(0.082)$\\
\end{tabular}
\end{table}
\end{frame}

\begin{frame}[label=sec-3-2]{Results: Price-adjusted Elasticity}
\begin{table}[htb]
\caption{\label{tab-find_phis}Vectors of Frisch elasticities by survey, scaled by different $\varphi$ to match pooled estimates as closely as possible.}
\centering
\begin{tabular}{lllll}
Goods & Pooled & BGD & GHA & SSD\\
\hline
Vegetables & $0.903$ & $0.872$ & $0.037$ & $0.775$\\
 & $(0.027)$ & $(0.026)$ & $(0.029)$ & $(3.972)$\\
Meat & $0.885$ & $0.949$ & $-0.072$ & $0.367$\\
 & $(0.209)$ & $(0.225)$ & $(0.356)$ & $(0.165)$\\
Sugar & $0.638$ & $0.721$ & $0.011$ & $0.782$\\
 & $(0.060)$ & $(0.092)$ & $(0.049)$ & $(3.385)$\\
Beans & $0.530$ & $0.496$ & $0.040$ & $0.364$\\
 & $(0.066)$ & $(0.078)$ & $(0.156)$ & $(1.701)$\\
Oil & $0.507$ & $0.437$ & $0.017$ & $0.886$\\
 & $(0.029)$ & $(0.029)$ & $(0.062)$ & $(4.545)$\\
\hline
$\varphi$ & $1.000$ & $0.940^{***}$ & $0.038^{***}$ & $0.955^{***}$\\
 & (---) & $(0.002)$ & $(0.010)$ & $(0.291)$\\
\end{tabular}
\end{table}
\end{frame}


\begin{frame}[label=sec-3-3]{Results: ATE's on price-adjusted $\log$$\lambda$$_{\text{it}}$}
\begin{center}
\begin{tabular}{llllll}
\hline
$\log$$\lambda$$_{\text{it}}$ & Midline & Endline &  & Mid (z-scr) & End (z-scr)\\
\hline
Bangladesh & $0.044^{**}$ & $-0.013$ &  & $0.141$ & $-0.216^{***}$\\
 & $(.014)$ & $(.013)$ &  & $(.049)$ & $(.05)$\\
South Sudan & $-.215^{*}$ & $0.077$ &  & $-0.34^{**}$ & $0.11$\\
 & $(.111)$ & $(.098)$ &  & $(.09)$ & $(.08)$\\
Ghana & $1.318$ & $2.76^{*}$ &  & ---- & ----\\
 & $(2.881)$ & $(1.525)$ &  &  & \\
\hline
\end{tabular}
\end{center}
\end{frame}

\begin{frame}[label=sec-3-4]{Results: ATE's on price-adjusted $\log$$\lambda$$_{\text{it}}$}
\begin{figure}[htb]
\centering
\includegraphics[width=.9\linewidth]{../Results/ate_lambda_price.png}
\caption{Average treatment effects on price-adjusted $\log$$\lambda$$_{\text{it}}$}
\end{figure}
\end{frame}

\begin{frame}[label=sec-3-5]{Results: ATE's on standardized $\log$$\lambda$$_{\text{it}}$}
\begin{figure}[htb]
\centering
\includegraphics[width=.9\linewidth]{../Results/ate_lambda_standardized.png}
\caption{Average treatment effects on standardized $\log$$\lambda$$_{\text{it}}$}
\end{figure}
\end{frame}



\begin{frame}[label=sec-3-6]{Results: ATE's on standardized aggregate consumption}
\begin{center}
\begin{tabular}{lll}
\hline
\$/day (z-scr) & Midline & Endline\\
\hline
Ghana & $0.097^{***}$ & $0.136^{***}$\\
 & $(.049)$ & $(.050)$\\
South Sudan & $0.34^{***}$ & $0.05$\\
 & $(.09)$ & $(.08)$\\
Bangladesh & $-0.025$ & $0.094^{***}$\\
 & $(.028)$ & $(.028)$\\
\hline
\end{tabular}
\end{center}
\end{frame}


\begin{frame}[label=sec-3-7]{Results: ATE's on standardized aggregate consumption}
\begin{figure}[htb]
\centering
\includegraphics[width=.9\linewidth]{../Results/ate_consumption.png}
\caption{Average treatment effects on standardized consumption}
\end{figure}
\end{frame}


\begin{frame}[label=sec-3-8]{Conclusion}
\begin{itemize}
\item Household welfare measures can't (shouldn't) be compared across context without some underlying theory

\item Thoughtful modeling frees us from otherwise prohibitive data requirements

\item We find that the TUP program had more impact in South Sudan, but only in the short run.

\item There are limits to what this particular method can do

\begin{itemize}
\item Big differences in level of aggregation mess up the price adjustment
\end{itemize}
\end{itemize}
\end{frame}
% Emacs 24.5.1 (Org mode 8.2.10)
\end{document}
